# firefox

Extensible web browser. https://mozilla.org

## Firefox Notices (at startup)
### Privacy
* https://www.mozilla.org/en-US/privacy/firefox/

### Welcome 1..10
* https://www.mozilla.org/en-US/firefox/welcome/1/
* https://www.mozilla.org/en-US/firefox/welcome/2/
* https://www.mozilla.org/en-US/firefox/welcome/3/
* https://www.mozilla.org/en-US/firefox/welcome/4/
* https://www.mozilla.org/en-US/firefox/welcome/5/
* https://www.mozilla.org/en-US/firefox/welcome/6/
* https://www.mozilla.org/en-US/firefox/welcome/7/
* https://www.mozilla.org/en-US/firefox/welcome/8/
* https://www.mozilla.org/en-US/firefox/welcome/9/
* https://www.mozilla.org/en-US/firefox/welcome/10/
* https://www.mozilla.org/en-US/firefox/welcome/11/ (Not yet!)
